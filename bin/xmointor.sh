#!/bin/bash
function stop(){
    ps -ef|grep xmonitor-client.jar|grep -v grep|awk '{print $2}'|xargs -r kill -9
}
function start(){
    has_=`ps -ef|grep  'xmonitor-client'|grep -v grep|awk '{print $2 }'`
    if [ "$has_" == "" ];then
            self_dir=$(cd "$(dirname "$0")"; pwd)
            cd ${self_dir}/../
            java -jar xmonitor-client.jar >> xmonitor.log 2>&1 &
            ps -ef|grep xmonitor-client.jar|grep -v grep
    else
       echo 'already started width pid:'$has_
    fi
}
function debug(){
    has_=`ps -ef|grep  'xmonitor-client'|grep -v grep|awk '{print $2 }'`
    if [ "$has_" == "" ];then
            self_dir=$(cd "$(dirname "$0")"; pwd)
            cd ${self_dir}/../
            java -jar xmonitor-client.jar
    else
       echo 'already started width pid:'$has_
    fi
}
function status(){
    ps -ef|grep xmonitor-client.jar|grep -v grep
}
case "$1" in
    start) 
    start  
    ;;
    stop)  
    stop   
    ;;
    status)
    status   
    ;;
    restart)
    stop   
    start  
    ;;
    debug)
    debug   
    ;;
    "")
    echo "Usage:/bin/bash xmonitor.sh (start|stop|status|restart)"
    ;;
esac

